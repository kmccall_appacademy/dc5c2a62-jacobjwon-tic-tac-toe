require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'

class Game
  attr_accessor :board, :player_one, :player_two, :current_player
  def initialize(player_one = HumanPlayer.new("John"),player_two = ComputerPlayer.new("Peter"))
    @player_one = player_one
    @player_two = player_two
    @board = Board.new
    @current_player = player_one
  end

  def play_turn

    move = @current_player.get_move

    @board.place_mark(move,@current_player.mark)
    switch_players!
  end


  def switch_players!
    
    if @current_player == player_one
      @current_player = player_two
    else
      @current_player = player_one
    end
  end

  def play
    until @board.over?
      play_turn
      @current_player.display(@board)
    end
  end

end

if __FILE__ == $PROGRAM_NAME
  game = Game.new
  game.play
end
